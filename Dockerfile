#### Metadata
FROM ubuntu:15.10
MAINTAINER Antoine Pourchet <antoine.pourchet@gmail.com>

#### Image Building
USER root
ENV HOME /root

# apt-get
RUN apt-get update
RUN apt-get install -y man git vim curl zsh python

RUN apt-get install -y golang
RUN apt-get install -y make
RUN apt-get install -y jq
RUN apt-get install -y screen

# Specifics
RUN apt-get install -y postgresql

# Go
ENV GOPATH /gopath
ENV PATH $GOPATH/bin:$PATH
RUN go get github.com/tools/godep

VOLUME /authenticator
VOLUME /certs
VOLUME /gopath

WORKDIR /authenticator
EXPOSE 9998
EXPOSE 443
EXPOSE 80

CMD godep restore; go run server.go
