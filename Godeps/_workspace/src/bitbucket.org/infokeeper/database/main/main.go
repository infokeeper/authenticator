package orm

import (
	"crypto/sha1"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

type DBConnManager struct {
	db *sql.DB
}

type Storable interface {
	Save() error
	Delete() error
}

type User struct {
	Id       int
	Email    string
	Pass_sha string
}

var manager = &DBConnManager{}

func NewUser(email, password string) error {
	shaBytes := sha1.Sum([]byte(password))
	shaString := fmt.Sprintf("%x", shaBytes)
	_, err := manager.db.Exec("INSERT INTO users (email, pass_sha) VALUES ($1, $2)", email, shaString)
	if err != nil {
		return err
	}
	return err
}

func GetUser(email, password string) (*User, error) {
	var u User
	shaString := fmt.Sprintf("%x", sha1.Sum([]byte(password)))
	err := manager.db.QueryRow("SELECT * FROM users WHERE email=$1 and pass_sha=$2", email, shaString).Scan(&u.Id, &u.Email, &u.Pass_sha)
	return &u, err
}

func UserExists(email string) bool {
	var tmp string
	err := manager.db.QueryRow("SELECT email FROM users WHERE email=$1", email).Scan(&tmp)
	if err == nil {
		return true
	} else {
		fmt.Println(err)
		return false
	}
}

func (u *User) Save() error {
	_, err := manager.db.Exec("UPDATE users SET ... WHERE id=$1", u.Id)
	return err
}

func (u *User) Delete() error {
	_, err := manager.db.Exec("DELETE FROM users WHERE id=$1", u.Email, u.Pass_sha)
	return err
}

func init() {
	db, err := sql.Open("postgres", "user=postgres dbname=test password=password")
	if err != nil {
		fmt.Println(err)
	}
	manager.db = db
}
