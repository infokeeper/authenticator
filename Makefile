.PHONY: all create start sysd-start stop restart attach

PROJECTNAME := authenticator
PORTS := -p 9998:9998
MOUNTS := -v `pwd`:/$(PROJECTNAME) -v /certs:/certs
ADDITIONAL := --net="host"
RUNARGS := $(PORTS) $(MOUNTS) $(ADDITIONAL)

default: create

create:
	docker build -t $(PROJECTNAME) .

start:
	docker run -it --name $(PROJECTNAME) $(RUNARGS) -d $(PROJECTNAME)

sysd-start:
	docker run -i --name $(PROJECTNAME) $(RUNARGS) $(PROJECTNAME)

sysd-restart:
	systemctl daemon-reload; \
	systemctl restart $(PROJECTNAME).service

stop:
	docker kill $(PROJECTNAME); docker rm $(PROJECTNAME); exit 0

restart: stop start

attach:
	docker exec -i -t $(PROJECTNAME) bash

remote-reload:
	ssh root@antoinepourchet.com 'systemctl daemon-reload; systemctl restart $(PROJECTNAME).service'
