package main

import (
	"bitbucket.org/infokeeper/database/main"
	"fmt"
	_ "github.com/lib/pq"
	"net/http"
)

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	email, ok1 := r.Form["email"]
	pass, ok2 := r.Form["password"]
	if ok1 && ok2 {
		err := orm.NewUser(email[0], pass[0])
		if err != nil {
			fmt.Println(err)
			fmt.Fprintf(w, `{"success": 0}`)
		} else {
			fmt.Fprintf(w, `{"success": 1}`)
		}
	} else {
		fmt.Fprintf(w, `{"success": 0, error='Invalid Form'}`)
	}
}

func AuthenticateHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	for key, val := range r.Form {
		fmt.Println(key, val)
	}
	fmt.Fprintf(w, `{"success": 1}`)
}

func TwoFactorHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	for key, val := range r.Form {
		fmt.Println(key, val)
	}
	fmt.Fprintf(w, `{"success": 1}`)
}

func CheckTwoFactorKeys(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	w.Header().Set("Access-Control-Allow-Origin", "*")
	for key, val := range r.Form {
		fmt.Println(key, val)
	}
	fmt.Fprintf(w, `{"success": 1}`)
}

func StatusHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `{"success": 1}`)
}

func TestHandler(w http.ResponseWriter, r *http.Request) {
	u, err := orm.GetUser("test@email.com", "password")
	if err != nil {
		fmt.Fprintf(w, err.Error())
	} else {
		fmt.Fprintf(w, u.Email)
	}
}

func main() {
	http.HandleFunc("/_status", StatusHandler)
	http.HandleFunc("/_test", TestHandler)
	http.HandleFunc("/register", RegisterHandler)
	http.HandleFunc("/authenticate", AuthenticateHandler)
	http.HandleFunc("/twofactor", TwoFactorHandler)
	http.HandleFunc("/checktwofactorkeys", CheckTwoFactorKeys)

	if err := http.ListenAndServe(":9998", nil); err != nil {
		fmt.Println("WTF happened?")
	}
}
